﻿using System.ComponentModel.DataAnnotations;

namespace Schedules.Models
{
    public class InviteForCreationDto
    {
        [Required(ErrorMessage = "You should provide a name value.")]
        public string? Name { get; set; }
        public string? Mail { get; set; }
    }
}
