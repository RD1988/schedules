﻿namespace Schedules.Models
{
    public class Invite
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Mail { get; set; }
        
    }
}

