﻿namespace Schedules.Models
{
    public class SchedulesForCreationDto
    {

        public string? Title { get; set; }
        public string Place { get; set; }
        // public DateTime Date { get; set; }

        public int Duration { get; set; }

        public string? Description { get; set; }


    }
}
