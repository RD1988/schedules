﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Schedules.Models;

namespace Schedules.Controllers
{
    [Route("api/schedules/{scheduleId}/invite")]
    [ApiController]
    public class InviteController : ControllerBase
    {

        
        [HttpGet("{inviteid}", Name = "GetInvite")]
        public ActionResult<IEnumerable<Invite>> GetInvites(int scheduleId)
        {
            var schedules = DataStore.Current.Schedules.FirstOrDefault(c => c.Id == scheduleId);

            if (schedules == null)
            {
                return NotFound();
            }

            return Ok(schedules.Invites);

        }

        [HttpPost]
        public ActionResult<InviteDto> CreateInvite(int scheduleId, InviteForCreationDto createInvite)
        {
            var schedules = DataStore.Current.Schedules.FirstOrDefault(c => c.Id == scheduleId);
            if (schedules == null)
            {
                return NotFound();
            }

            var idCount = DataStore.Current.Schedules.Count();

            var postInvite = new InviteDto()
            {
                Id = ++ idCount,
                Name = createInvite.Name, 
                Mail = createInvite.Mail,

            };

            schedules.Invites.Add(postInvite);

             return CreatedAtRoute("GetInvite",
                 new
                 {
                     scheduleId = scheduleId,
                     pointOfInterestId = postInvite.Id,
                 },
                 postInvite);








        }





    }
}
