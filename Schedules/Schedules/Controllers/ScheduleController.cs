﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Schedules.Models;

namespace Schedules.Controllers
{
    [Route("api/schedules")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
       

        [HttpGet]
        public ActionResult<IEnumerable<ScheduleDto>> GetSchedules()
        {
            //returns a schedules
            return Ok(DataStore.Current.Schedules);
        }

        [HttpGet("{id}")]
        public ActionResult<ScheduleDto> GetSchedule(int id) 
        {
            var scheduleToReturn = DataStore.Current.Schedules.FirstOrDefault(c => c.Id == id);

            if (scheduleToReturn == null)
            {
                return NotFound();
            }

            return Ok(scheduleToReturn);    
        }


        [HttpPost]
        public ActionResult<Schedule> CreateSchedule(int scheduleId, ScheduleDto scheduleDto)
        {
            var schedule = DataStore.Current.Schedules.FirstOrDefault(c => c.Id == scheduleId);

            if (schedule == null)
            {
                NotFound();
            }

            var idCount = DataStore.Current.Schedules.Count();


            var addSchedule = new ScheduleDto()
            {
                Id = ++idCount,
                Title = scheduleDto.Title,
                Place = scheduleDto.Place,
                //   Date = scheduleDto.Date,
                //Duration = scheduleDto.Duration,
                Description = scheduleDto.Description,
                Name = scheduleDto.Name,
                Mail = scheduleDto.Mail,

            };


            DataStore.Current.Schedules.Add(addSchedule);


            return CreatedAtRoute(
                new
                {

                    scheduleId = addSchedule.Id,

                },
                addSchedule);

        }


        // [HttpGet("{scheduleId}")]
        // public ActionResult<Schedule> GetSchedules( int scheduleId)
        //{
        //     var scheduleToReturn = DataStore.Current.Schedules.FirstOrDefault(c => c.Id == scheduleId);

        //     if (scheduleToReturn == null)
        //     {
        //         return NotFound();
        //     }

        //     // find point of interest

        //     return Ok(scheduleToReturn);
        // }

    }

}



