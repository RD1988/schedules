﻿using Schedules.Models;

namespace Schedules
{
    public class DataStore
    {
        public List<ScheduleDto> Schedules { get; set; }

     

        public static DataStore Current { get; } = new DataStore();




        public DataStore()
        {



            Schedules = new List<ScheduleDto>()
            {


                new ScheduleDto()
                {
                    Id = 1,
                    Title = "Brætspil",
                    Description =  "Beskrielse",
                    Invites = new List<InviteDto>
                    {
                        new InviteDto()
                        {
                            Id=1,   
                            Mail = "rene@gmail.com",
                            Name = "Rene"
                        }  ,

                             new InviteDto()
                        {
                                 Id=2,  
                            Mail = "sine@gmail.com",
                            Name = "Sine"
                        }


                    }

                },
                  new ScheduleDto()
                {
                    Id = 2,
                    Title = "Boldspil",
                    Description =  "Beskrielse",
                    Invites = new List<InviteDto>
                    {
                        new InviteDto()
                        {
                            Id= 3, 
                            Mail = "henrik@gmail.com",
                            Name = "Henrik"
                        }  ,

                             new InviteDto()
                        {
                                 Id = 4,
                            Mail = "jette@gmail.com",
                            Name = "Jette"
                        }


                    }

                }

            };
        }
    }
}
