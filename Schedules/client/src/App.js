import React, { Component } from "react";
import Nav from "./components/Nav";
import "./custom.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";
import Home from "./pages/Home";
import Schedule from "./pages/Schedule";
import Schedules from "./pages/Schedules";
import Join from "./pages/Join";
import NoPage from "./pages/NoPage";




export class App extends Component {

  constructor(props) {
    super(props);

    this.state =
    {

      //empty array.
      schedules: []


    };
  }


  // getSchedulesById(Id) {
  //   return this.state.schedules.find(q => q.Id === Id);
  // }



  async schedulePost(title, place, startDate, description, name, mail) {

    //Post request here, to the backend server

    let url = "https://localhost:7216/api/schedules";

    await fetch(url, {
      method: "POST",
      body: JSON.stringify({
        title: title,
        place: place,
        startDate: startDate,
        description: description,
        name: name,
        mail: mail
      }),
      headers: {
        "Content-type": "application/json;"
      }
    })
      .then(response => response.json())
      .then(json => {
        console.log("Result of posting a new question:");
        console.log(json);

      });
  }









  componentDidMount() {
    this.schedulesGet();

  }

  async schedulesGet() {

    let url = "https://localhost:7216/api/schedules";


    await fetch(url).then(res =>
      res.json().then(schedules => this.setState({ schedules }))
    );



    //console.log(this.state.schedules);
  }





  render() {



    return (
      <div>


        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Layout />}>
              <Route index element={<Home />} />
              <Route path="Schedule" element={<Schedule postSchedule={(title, place, startDate, description, name, mail) => this.schedulePost(title, place, startDate, description, name, mail)} />} />
              <Route path="/Schedules/:id" element={<Schedules />} />

              <Route path="join" element={<Join schedulesGet={this.state.schedules} />} />
              <Route path="*" element={<NoPage />} />
            </Route>
          </Routes>
        </BrowserRouter>


      </div >


    );
  }

}

export default App;
