import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Join extends Component {

    constructor(props) {
        super(props);
    }


    //

    render() {

        console.log(this.props.schedulesGet);

        return (<div>
            <div className="container mt-5">

                <h1>Find aftale</h1>


                {this.props.schedulesGet.map(q => (

                    <div key={q.id} className="card mt-5" >

                        <div className="card-body">
                            <h5 className="card-title">{q.title}</h5>
                            <p className="card-text">{q.description}</p>

                            <Link className="btn btn-primary stretched-link" to={`/schedules/${q.id}`}>{q.title}</Link>

                        </div>
                    </div>


                ))}



            </div>
        </div>);
    }


}

// const Join = () => {
//     return <h1>Join me</h1>;
// };

export default Join;