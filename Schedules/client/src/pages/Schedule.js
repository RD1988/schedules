import React, { Component } from "react";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";



export class Schedule extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: "",
            place: "",
            startDate: new Date(),
            description: "",
            name: "",
            mail: ""
        };
        this.handleChangeDate = this.handleChangeDate.bind(this);
        // this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    componentDidMount() {
        //callpost




    }


    handleChangeDate(date) {
        this.setState({
            startDate: date
        })
    }


    //if the value chance?
    handlePostScheduleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }



    //arrow function.
    postSchedule = e => {

        // without preventDefault, the browser will refresh, and thats not what we want.


        // this.props.postschedule(this.state.title, this.state.place);

        //  console.log(this.state.title, this.state.place, this.state.startDate, this.state.description, this.state.name, this.state.mail);

        this.props.postSchedule(
            this.state.title,
            this.state.place,
            this.state.startDate,
            this.state.description,
            this.state.name,
            this.state.mail
        );

        e.preventDefault();





    }


    render() {

        console.log(this.props.postSchedule);

        return (
            <div>
                <div className="container mt-5">

                    <div className="card" >
                        <div className="card-body">
                            <h5 className="card-title">Opret en aftale</h5>
                            <form onSubmit={this.postSchedule}>
                                <div className="form-group">
                                    <label>Titel</label>

                                    <input type="text" name="title" className="form-control" onChange={event => this.handlePostScheduleChange(event)} aria-describedby="emailHelp" placeholder="Hvad skal din aftale hedde?" />

                                </div>

                                <div className="form-group">
                                    <label>Sted</label>

                                    <input type="text" name="place" className="form-control" onChange={event => this.handlePostScheduleChange(event)} aria-describedby="emailHelp" placeholder="Hvor skal aftales holdes henne?" />

                                </div>

                                <div className="form-group">
                                    <label>Dato</label>
                                    <DatePicker
                                        selected={this.state.startDate}
                                        onChange={this.handleChangeDate}
                                        name="startDate"
                                        dateFormat="MM/dd/yyyy"

                                    />


                                </div>

                                <div className="form-group">
                                    <label>Varighed</label>



                                </div>

                                <div className="form-group">
                                    <label>Beskrivelse</label>

                                    <input type="text" name="description" className="form-control" onChange={event => this.handlePostScheduleChange(event)} placeholder="Beskriv gerne hvad du vil lave på denne aftale, fx boldspil, drikke kaffe eller noget andet." />

                                </div>
                                <div className="form-group">
                                    <label>Navn</label>

                                    <input type="text" name="name" className="form-control" onChange={event => this.handlePostScheduleChange(event)} placeholder="Indtast dit fulde navn" />

                                </div>

                                <div className="form-group">
                                    <label>E-mail adresse</label>

                                    <input type="text" name="mail" className="form-control" onChange={event => this.handlePostScheduleChange(event)} placeholder="Indtast din e-mail adresse" />

                                </div>




                                <button className="btn btn-primary">
                                    Opret en aftale
                                </button>



                            </form>


                        </div>
                    </div>


                </div>
            </div>
        );
    }


}

export default Schedule;