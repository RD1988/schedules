import React, { Component } from "react";
import { Outlet, Link } from "react-router-dom";
import schedules from '../img/schedules.png';
import join from '../img/joinschedule.png';




// 

export class Home extends Component {



    render() {
        return (<div>  <div className="jumbotron">
            <div className="row">
                <div className="col-6 text-center"><h1>Har du en aftale?</h1>

                    <Link to="/schedule" className="btn btn-primary">Opret aftale</Link>
                </div>
                <div className="col-6">

                    <img src={schedules}></img>
                </div>
            </div>

        </div>
            <div className="jumbotron bg-white text-black">
                <div className="row">
                    <div className="col-6 text-center">

                        <img src={join}></img>
                    </div>
                    <div className="col-6">
                        <h1>Find igangværende aftaler</h1>
                        <Link to="/join" className="btn btn-primary">
                            Find igangværende aftaler</Link>
                    </div>

                </div>
            </div></div>);
    }



}



export default Home;