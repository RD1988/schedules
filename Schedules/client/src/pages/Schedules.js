import React, { Component } from "react";
import { useParams } from 'react-router-dom';

export class Schedules extends Component {

    constructor(props) {
        super(props);

        this.state =
        {

            //empty array.
            schedules: [],
            id: window.location.pathname.split("/").pop()


        };




    }

    componentDidMount() {

        const url = "https://localhost:7216/api/schedules/" + this.state.id;

        fetch(url).then(res =>
            res.json().then(schedules => this.setState({ schedules }))
        );

    }





    render() {



        console.log(this.state.schedules)

        return (<div>

            <div className="container mt-5">

                <div className="card">
                    <div className="card-header">
                        {this.state.schedules.title}
                    </div>
                    <div className="card-body">
                        <a href="#" className="btn btn-primary float-right">Del aftale</a>
                        <h5 className="card-title"> {this.state.schedules.place}</h5>
                        <p className="card-text">{this.state.schedules.description}</p>
                        <p className="card-text"><small className="text-muted">Aftalen er oprettet af : {this.state.schedules.name}</small></p>

                    </div>
                </div>







            </div>


        </div>)

    }


}

export default Schedules;
