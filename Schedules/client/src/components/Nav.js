import React, { Component } from "react";
import logo from '../img/logo.png';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "../pages/Layout";
import Home from "../pages/Home";
import Schedule from "../pages/Schedule";
import Schedules from "../pages/Schedules";
import Join from "../pages/Join";
import NoPage from "../pages/NoPage";





export class Nav extends Component {
    constructor(props) {
        super(props);

    }

    async schedulePost(title, place, startDate, description, name, mail) {
        //  console.log(title, place);

        this.props.postSchedule(title, place, startDate, description, name, mail)

    }

    // scheduleGetById(Id) {


    //     return this.props.scheduleGetById(Id);



    // }




    render() {




        return (


            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<Home />} />
                        <Route path="Schedule" element={<Schedule postSchedule={(title, place, startDate, description, name, mail) => this.schedulePost(title, place, startDate, description, name, mail)} />} />
                        <Route path="Schedules/:id" element={<Schedules />} />

                        <Route path="join" element={<Join schedulesGet={this.props.schedulesGet} />} />
                        <Route path="*" element={<NoPage />} />
                    </Route>
                </Routes>
            </BrowserRouter>



        );
    }
}
export default Nav;